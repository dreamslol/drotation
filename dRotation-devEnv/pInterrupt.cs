﻿using System;
using robotManager.Helpful;
using robotManager.Products;
using wManager.Wow.Helpers;
using wManager.Wow.Bot;
using wManager.Wow.Class;
using wManager.Wow.Enums;
using wManager.Wow.ObjectManager;
using System.Collections.Generic;
using System.Threading;

public class Main_INT : wManager.Plugin.IPlugin

{
    private static List<int> InterruptSpells = new List<int>(); /* create list with spells */

    /* Interrupt that will be used */
    public Spell Kick = new Spell("Kick");

    /* Code here runs when the plugin is enabled and the bot it started. */
    public void Initialize()
    {
        /* Spells that will be interrupted */
        InterruptSpells.Add(48785); // Flash of Light
        InterruptSpells.Add(48782); // Holy Light

        foreach (int i in InterruptSpells)
        {
            if (Kick.KnownSpell && Kick.IsSpellUsable && Kick.IsDistanceGood && Fight.InFight && ObjectManager.Target.CanInterruptCasting﻿)
            {
                Thread.Sleep(Others.Random(0, ObjectManager.Target.CastingTimeLeft - 350));
                Kick.Launch();
                Thread.Sleep(SpellManager.GlobalCooldownTimeLeft());
                Logging.Write("Found something..lets try");
                return;
            }
        }
    }

    public void Dispose()
    {
        /* Code to run when the bot is stopped */
    }

    public void Settings()
    {
        /* Code to run when someone clicks the plugin settings button */

    }
}
