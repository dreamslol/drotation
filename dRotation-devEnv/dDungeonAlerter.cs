﻿using robotManager.Helpful;
using wManager.Wow.Enums;
using wManager.Wow.Helpers;
using wManager.Events;
using System.Media;
using System.Windows.Forms;

public class Main_DA : wManager.Plugin.IPlugin
{
    private bool _isRunning;

    /* Code here runs when the plugin is enabled and the bot it started. */
    public void Initialize()
    {
        _isRunning = true;
        Logging.Write("[dDungeonAlerter] Loaded.");

        EventsLua.AttachEventLua(LuaEventsId.LFG_PROPOSAL_SHOW, OnInviteRequest); // LFG invite
    }

    public void OnInviteRequest(object context)
    {

        string path = Application.StartupPath;

        // Create new SoundPlayer in the using statement.
        SoundPlayer notification = new SoundPlayer(path + @"\Plugins\dDungeonAlerter\ready.wav");

        try
        {
            if (Conditions.InGameAndConnectedAndAliveAndProductStartedNotInPause)
            {
                Lua.LuaDoString("LFDDungeonReadyDialogEnterDungeonButton:Click()"); // Accept Dungeon invite
                notification.PlaySync(); // Play the sound.
            }
        }
        catch
        {
        }
    }

    public void Dispose()
    {
        _isRunning = false;
        Logging.Write("[dDungeonAlerter] Stopped.");
    }

    public void Settings()
    {
        Logging.Write("[dDungeonAlerter] No settings.");
    }
}