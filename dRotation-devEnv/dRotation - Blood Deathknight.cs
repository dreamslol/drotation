﻿using System;
using System.Threading;
using robotManager.Helpful;
using robotManager.Products;
using wManager.Wow.Class;
using wManager.Wow.Enums;
using wManager.Wow.Helpers;
using wManager.Wow.ObjectManager;

public class Main_BD : ICustomClass
{
    public string name = "dRotation - Blood Deathknight";
    public float Range { get { return 5.0f; } }

    private bool _isRunning;

    /*
     * Initialize()
     * When product started, initialize and launch Fightclass
     */
    public void Initialize()
    {
        _isRunning = true;
        Logging.Write(name + " Is initialized.");
        CreateStatusFrame();
        Rotation();
    }

    /*
     * Dispose()
     * When product stopped
     */
    public void Dispose()
    {
        _isRunning = false;
        Logging.Write(name + " Stop in progress.");
        Lua.LuaDoString(@"dRotationFrame.text:SetText(""dRotation Stopped!"")");
    }

    /*
     * ShowConfiguration()
     * When use click on Fightclass settings
     */
    public void ShowConfiguration()
    {
        Logging.Write(name + " No settings for this Fightclass.");
    }

    /* Spells for Rotation */
    public Spell IcyTouch = new Spell("Icy Touch");
    public Spell PlagueStrike = new Spell("Plague Strike");
    public Spell HeartStrike = new Spell("Heart Strike");
    public Spell DeathStrike = new Spell("Death Strike");
    public Spell Deathcoil = new Spell("Deathcoil");

    /* AOE */
    public Spell DeathAndDecay = new Spell("Death and Decay");
    public Spell Pestilance = new Spell("Pestilance");

    /* Rotation() */
    public void Rotation()
    {
        Logging.Write(name + ": Started.");

        while (_isRunning)
        {
            try
            {
                if (Products.InPause)
                {
                    Lua.LuaDoString(@"dRotationFrame.text:SetText(""dRotation Paused!"")");
                }

                if (!Products.InPause)
                {
                    if (!ObjectManager.Me.IsDeadMe)
                    {
                        if (!ObjectManager.Me.InCombat)
                        {
                            Lua.LuaDoString(@"dRotationFrame.text:SetText(""dRotation Active!"")");
                        }
                        else if (ObjectManager.Me.InCombat && ObjectManager.Me.Target > 0)
                        {
                            CombatRotation();
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Logging.WriteError(name + " ERROR: " + e);
            }

            Thread.Sleep(10); // Pause 10 ms to reduce the CPU usage.
        }
        Logging.Write(name + ": Stopped.");
    }

    /*
     * CreateStatusFrame()
     * InGame Status frame to see which spells casting next
     */
    public void CreateStatusFrame()
    {
        Lua.LuaDoString(@"
        if not dRotationFrame then
            dRotationFrame = CreateFrame(""Frame"")
            dRotationFrame:ClearAllPoints()
            dRotationFrame:SetBackdrop(StaticPopup1:GetBackdrop())
            dRotationFrame:SetHeight(70)
            dRotationFrame:SetWidth(210)

            dRotationFrame.text = dRotationFrame:CreateFontString(nil, ""BACKGROUND"", ""GameFontNormal"")
            dRotationFrame.text:SetAllPoints()
            dRotationFrame.text:SetText(""dRotation by Dreamful, Ready!"")
            dRotationFrame.text:SetTextColor(1, 1, 1, 6)
            dRotationFrame:SetPoint(""CENTER"", 0, -240)
            dRotationFrame:SetBackdropBorderColor(0, 0, 0, 0)

            dRotationFrame:SetMovable(true)
            dRotationFrame:EnableMouse(true)
            dRotationFrame:SetScript(""OnMouseDown"",function() dRotationFrame:StartMoving() end)
            dRotationFrame:SetScript(""OnMouseUp"",function() dRotationFrame:StopMovingOrSizing() end)

            dRotationFrame.Close = CreateFrame(""BUTTON"", nil, dRotationFrame, ""UIPanelCloseButton"")
            dRotationFrame.Close:SetWidth(15)
            dRotationFrame.Close:SetHeight(15)
            dRotationFrame.Close:SetPoint(""TOPRIGHT"", dRotationFrame, -8, -8)
            dRotationFrame.Close:SetScript(""OnClick"", function()
                dRotationFrame:Hide()
                DEFAULT_CHAT_FRAME:AddMessage(""dRotationStatusFrame |cffC41F3Bclosed |cffFFFFFFWrite /dRotation to enable again."") 	
            end)

            SLASH_WHATEVERYOURFRAMESARECALLED1=""/dRotation""
            SlashCmdList.WHATEVERYOURFRAMESARECALLED = function()
                if dRotationFrame:IsShown() then
                    dRotationFrame:Hide()
                else
                    dRotationFrame:Show()
                end
            end
        end");
    }

    /* CombatRotation() */
    public void CombatRotation()
    {
        // Icy Touch
        if (IcyTouch.KnownSpell && IcyTouch.IsSpellUsable && IcyTouch.IsDistanceGood && !ObjectManager.Target.HaveBuff("Icy Touch"))
        {
            Lua.LuaDoString(@"dRotationFrame.text:SetText(""Casting Icy Touch"")");
            IcyTouch.Launch();
            return;
        }

        // Plague Strike
        if (PlagueStrike.KnownSpell && PlagueStrike.IsSpellUsable && PlagueStrike.IsDistanceGood && !ObjectManager.Target.HaveBuff("Plague Strike"))
        {
            Lua.LuaDoString(@"dRotationFrame.text:SetText(""Casting Plague Strike"")");
            PlagueStrike.Launch();
            return;
        }

        // Death Strike
        if (DeathStrike.KnownSpell && DeathStrike.IsSpellUsable && DeathStrike.IsDistanceGood && ObjectManager.Me.HealthPercent <= 80)
        {
            Lua.LuaDoString(@"dRotationFrame.text:SetText(""Casting Death Strike"")");
            DeathStrike.Launch();
            return;
        }

        // Deathcoil
        if (Deathcoil.KnownSpell && Deathcoil.IsSpellUsable && Deathcoil.IsDistanceGood && ObjectManager.Me.RunicPower >= 40)
        {
            Lua.LuaDoString(@"dRotationFrame.text:SetText(""Casting Deathcoil"")");
            Deathcoil.Launch();
            return;
        }

        // Death and Decay
        if (DeathAndDecay.KnownSpell && DeathAndDecay.IsSpellUsable && DeathAndDecay.IsDistanceGood && ObjectManager.GetUnitAttackPlayer().Count >= 2)
        {
            Lua.LuaDoString(@"dRotationFrame.text:SetText(""Casting Death and Decay"")");
            SpellManager.CastSpellByIDAndPosition(12345, ObjectManager.Target.Position); // casting at target 
            return;
        }

        // Pestilance
        if (Pestilance.KnownSpell && Pestilance.IsSpellUsable && Pestilance.IsDistanceGood && ObjectManager.GetUnitAttackPlayer().Count >= 3 && ObjectManager.Target.HaveBuff("Plague Strike") && ObjectManager.Target.HaveBuff("Icy Touch"))
        {
            Lua.LuaDoString(@"dRotationFrame.text:SetText(""Casting Pestilance"")");
            Pestilance.Launch();
            return;
        }

        // Heart Strike
        if (HeartStrike.KnownSpell && HeartStrike.IsSpellUsable && HeartStrike.IsDistanceGood)
        {
            Lua.LuaDoString(@"dRotationFrame.text:SetText(""Casting Heart Strike"")");
            HeartStrike.Launch();
            return;
        }

    }
}