﻿using System;
using System.Linq;
using System.Threading;
using robotManager.Helpful;
using robotManager.Products;
using wManager.Wow.Class;
using wManager.Wow.Enums;
using wManager.Wow.Helpers;
using wManager.Wow.ObjectManager;

public class Main_FWW : ICustomClass
{
    public string name = "dRotation - Fury Warrior";
    public float Range { get { return 5.0f; } }

    private bool _isRunning;

    /*
     * Initialize()
     * When product started, initialize and launch Fightclass
     */
    public void Initialize()
    {
        _isRunning = true;
        Logging.Write(name + " Is initialized.");
        CreateStatusFrame();
        Rotation();
    }

    /*
     * Dispose()
     * When product stopped
     */
    public void Dispose()
    {
        _isRunning = false;
        Logging.Write(name + " Stop in progress.");
        Lua.LuaDoString(@"dRotationFrame.text:SetText(""dRotation Stopped!"")");
    }

    /*
     * ShowConfiguration()
     * When use click on Fightclass settings
     */
    public void ShowConfiguration()
    {
        Logging.Write(name + " No settings for this Fightclass.");
    }

    /* Buffs */
    public Spell BattleShout = new Spell("Battle Shout");
    public Spell BeserkerStance = new Spell("Berserker Stance");

    /* Spells for Rotation */
    public Spell Charge = new Spell("Charge");
    public Spell Bloodrage = new Spell("Bloodrage");
    public Spell Bloodthirst = new Spell("Bloodthirst");
    public Spell Whirlwind = new Spell("Whirlwind");
    public Spell Slam = new Spell("Slam");
    public Spell Recklessness = new Spell("Recklessness");
    public Spell DeathWish = new Spell("DeathWish");
    public Spell Execute = new Spell("Execute");
    public Spell HeroicThrow = new Spell("Heroic Throw");
    public Spell Cleave = new Spell("Cleave");
    public Spell HeroicStrike = new Spell("Heroic Strike");

    /* Rotation() */
    public void Rotation()
    {
        Logging.Write(name + ": Started.");

        while (_isRunning)
        {
            try
            {
                if (Products.InPause)
                {
                    Lua.LuaDoString(@"dRotationFrame.text:SetText(""dRotation Paused!"")");
                }

                if (!Products.InPause)
                {
                    Buff();
                    if (!ObjectManager.Me.IsDeadMe)
                    {
                        if (!ObjectManager.Me.InCombat)
                        {
                            Lua.LuaDoString(@"dRotationFrame.text:SetText(""dRotation Active!"")");
                        }
                        else if (ObjectManager.Me.InCombat && ObjectManager.Me.Target > 0)
                        {
                            CombatRotation();
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Logging.WriteError(name + " ERROR: " + e);
            }

            Thread.Sleep(10); // Pause 10 ms to reduce the CPU usage.
        }
        Logging.Write(name + ": Stopped.");
    }

    /*
     * CreateStatusFrame()
     * InGame Status frame to see which spells casting next
     */
    public void CreateStatusFrame()
    {
        Lua.LuaDoString(@"
        if not dRotationFrame then
            dRotationFrame = CreateFrame(""Frame"")
            dRotationFrame:ClearAllPoints()
            dRotationFrame:SetBackdrop(StaticPopup1:GetBackdrop())
            dRotationFrame:SetHeight(70)
            dRotationFrame:SetWidth(210)

            dRotationFrame.text = dRotationFrame:CreateFontString(nil, ""BACKGROUND"", ""GameFontNormal"")
            dRotationFrame.text:SetAllPoints()
            dRotationFrame.text:SetText(""dRotation by Dreamful, Ready!"")
            dRotationFrame.text:SetTextColor(1, 1, 1, 6)
            dRotationFrame:SetPoint(""CENTER"", 0, -240)
            dRotationFrame:SetBackdropBorderColor(0, 0, 0, 0)

            dRotationFrame:SetMovable(true)
            dRotationFrame:EnableMouse(true)
            dRotationFrame:SetScript(""OnMouseDown"",function() dRotationFrame:StartMoving() end)
            dRotationFrame:SetScript(""OnMouseUp"",function() dRotationFrame:StopMovingOrSizing() end)

            dRotationFrame.Close = CreateFrame(""BUTTON"", nil, dRotationFrame, ""UIPanelCloseButton"")
            dRotationFrame.Close:SetWidth(15)
            dRotationFrame.Close:SetHeight(15)
            dRotationFrame.Close:SetPoint(""TOPRIGHT"", dRotationFrame, -8, -8)
            dRotationFrame.Close:SetScript(""OnClick"", function()
                dRotationFrame:Hide()
                DEFAULT_CHAT_FRAME:AddMessage(""dRotationStatusFrame |cffC41F3Bclosed |cffFFFFFFWrite /dRotation to enable again."") 	
            end)

            SLASH_WHATEVERYOURFRAMESARECALLED1=""/dRotation""
            SlashCmdList.WHATEVERYOURFRAMESARECALLED = function()
                if dRotationFrame:IsShown() then
                    dRotationFrame:Hide()
                else
                    dRotationFrame:Show()
                end
            end
        end");
    }

    /* Buff() */
    public void Buff()
    {
        // Batlle Shout
        if (BattleShout.KnownSpell && BattleShout.IsSpellUsable && !ObjectManager.Me.HaveBuff(47436) && !ObjectManager.Me.HaveBuff(48934) && !ObjectManager.Me.HaveBuff(48932) && ObjectManager.Me.Rage >= 10)
        {
            Lua.LuaDoString(@"dRotationFrame.text:SetText(""Casting Battle Shout"")");
            BattleShout.Launch();
            return;
        }

        // Berserker Stance
        if (BeserkerStance.KnownSpell && BeserkerStance.IsSpellUsable && !ObjectManager.Me.HaveBuff(2458))
        {
            Lua.LuaDoString(@"dRotationFrame.text:SetText(""Casting Berserker Stance"")");
            BeserkerStance.Launch();
            return;
        }
    }

    /* CombatRotation() */
    public void CombatRotation()
    {

        // check targets from target position
        var unitsAroundTarget = ObjectManager.GetWoWUnitAttackables().Count(u => u.Position.DistanceTo(ObjectManager.Target.Position) <= 6 && u.IsAttackable);

        // Eviscerate
        if (Bloodrage.KnownSpell && Bloodrage.IsSpellUsable && Bloodrage.IsDistanceGood && ObjectManager.Me.Rage <= 65)
        {
            Lua.LuaDoString(@"dRotationFrame.text:SetText(""Casting Bloodrage"")");
            Bloodrage.Launch();
            return;
        }

        if (Bloodthirst.KnownSpell && Bloodthirst.IsSpellUsable && Bloodthirst.IsDistanceGood)
        {
            Lua.LuaDoString(@"dRotationFrame.text:SetText(""Casting Bloodthirst"")");
            Bloodthirst.Launch();
            return;
        }

        if (Whirlwind.KnownSpell && Whirlwind.IsSpellUsable && Whirlwind.IsDistanceGood)
        {
            Lua.LuaDoString(@"dRotationFrame.text:SetText(""Casting Whirlwind"")");
            Whirlwind.Launch();
            return;
        }

        if (Slam.KnownSpell && Slam.IsSpellUsable && Slam.IsDistanceGood && ObjectManager.Me.HaveBuff(46916))
        {
            Lua.LuaDoString(@"dRotationFrame.text:SetText(""Casting Slam"")");
            Slam.Launch();
            return;
        }

        if (Recklessness.KnownSpell && Recklessness.IsSpellUsable && Recklessness.IsDistanceGood && ObjectManager.Target.IsBoss)
        {
            Lua.LuaDoString(@"dRotationFrame.text:SetText(""Casting Recklessness"")");
            Recklessness.Launch();
            return;
        }

        if (DeathWish.KnownSpell && DeathWish.IsSpellUsable && DeathWish.IsDistanceGood && ObjectManager.Target.IsBoss)
        {
            Lua.LuaDoString(@"dRotationFrame.text:SetText(""Casting Death Wish"")");
            DeathWish.Launch();
            return;
        }

        if (Execute.KnownSpell && Execute.IsSpellUsable && Execute.IsDistanceGood && ObjectManager.Target.HealthPercent <= 20)
        {
            Lua.LuaDoString(@"dRotationFrame.text:SetText(""Casting Execute"")");
            Execute.Launch();
            return;
        }

        if (HeroicThrow.KnownSpell && HeroicThrow.IsSpellUsable && HeroicThrow.IsDistanceGood)
        {
            Lua.LuaDoString(@"dRotationFrame.text:SetText(""Casting Heroic Throw"")");
            HeroicThrow.Launch();
            return;
        }

        if (Cleave.KnownSpell && Cleave.IsSpellUsable && Cleave.IsDistanceGood && ObjectManager.Me.Rage >= 65 && unitsAroundTarget >= 2)
        {
            Lua.LuaDoString(@"dRotationFrame.text:SetText(""Casting Cleave"")");
            Cleave.Launch();
            return;
        }

        if (HeroicStrike.KnownSpell && HeroicStrike.IsSpellUsable && HeroicStrike.IsDistanceGood && ObjectManager.Me.Rage >= 65 && unitsAroundTarget <= 2)
        {
            Lua.LuaDoString(@"dRotationFrame.text:SetText(""Casting Heroic Strike"")");
            HeroicStrike.Launch();
            return;
        }
    }
}