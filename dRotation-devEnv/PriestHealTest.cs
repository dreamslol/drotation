﻿using System;
using System.Threading;
using robotManager.Helpful;
using robotManager.Products;
using wManager.Wow.Class;
using wManager.Wow.Enums;
using wManager.Wow.Helpers;
using wManager.Wow.ObjectManager;

public class Main_PH : ICustomClass
{
    public string name = "Template";
    public float Range { get { return 30.0f; } }

    private bool _isRunning;

    /*
     * Initialize()
     * When product started, initialize and launch Fightclass
     */
    public void Initialize()
    {
        _isRunning = true;
        Logging.Write(name + " Is initialized.");
        CreateStatusFrame();
        Rotation();
    }

    /*
     * Dispose()
     * When product stopped
     */
    public void Dispose()
    {
        _isRunning = false;
        Logging.Write(name + " Stop in progress.");
        Lua.LuaDoString(@"dRotationFrame.text:SetText(""dRotation Stopped!"")");
    }

    /*
     * ShowConfiguration()
     * When use click on Fightclass settings
     */
    public void ShowConfiguration()
    {
        Logging.Write(name + " No settings for this Fightclass.");
    }

    /* Rotation() */
    public void Rotation()
    {
        Logging.Write(name + ": Started.");

        while (_isRunning)
        {
            try
            {
                if (Products.InPause)
                {
                    Lua.LuaDoString(@"dRotationFrame.text:SetText(""dRotation Paused!"")");
                }

                if (!Products.InPause)
                {
                    if (!ObjectManager.Me.IsDeadMe)
                    {
                        if (!ObjectManager.Me.InCombat)
                        {
                            Lua.LuaDoString(@"dRotationFrame.text:SetText(""dRotation Active!"")");
                            CombatRotation();
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Logging.WriteError(name + " ERROR: " + e);
            }

            Thread.Sleep(10); // Pause 10 ms to reduce the CPU usage.
        }
        Logging.Write(name + ": Stopped.");
    }

    /*
     * CreateStatusFrame()
     * InGame Status frame to see which spells casting next
     */
    public void CreateStatusFrame()
    {
        Lua.LuaDoString(@"
        if not dRotationFrame then
            dRotationFrame = CreateFrame(""Frame"")
            dRotationFrame:ClearAllPoints()
            dRotationFrame:SetBackdrop(StaticPopup1:GetBackdrop())
            dRotationFrame:SetHeight(70)
            dRotationFrame:SetWidth(210)

            dRotationFrame.text = dRotationFrame:CreateFontString(nil, ""BACKGROUND"", ""GameFontNormal"")
            dRotationFrame.text:SetAllPoints()
            dRotationFrame.text:SetText(""dRotation by Dreamful, Ready!"")
            dRotationFrame.text:SetTextColor(1, 1, 1, 6)
            dRotationFrame:SetPoint(""CENTER"", 0, -240)
            dRotationFrame:SetBackdropBorderColor(0, 0, 0, 0)

            dRotationFrame:SetMovable(true)
            dRotationFrame:EnableMouse(true)
            dRotationFrame:SetScript(""OnMouseDown"",function() dRotationFrame:StartMoving() end)
            dRotationFrame:SetScript(""OnMouseUp"",function() dRotationFrame:StopMovingOrSizing() end)

            dRotationFrame.Close = CreateFrame(""BUTTON"", nil, dRotationFrame, ""UIPanelCloseButton"")
            dRotationFrame.Close:SetWidth(15)
            dRotationFrame.Close:SetHeight(15)
            dRotationFrame.Close:SetPoint(""TOPRIGHT"", dRotationFrame, -8, -8)
            dRotationFrame.Close:SetScript(""OnClick"", function()
                dRotationFrame:Hide()
                DEFAULT_CHAT_FRAME:AddMessage(""dRotationStatusFrame |cffC41F3Bclosed |cffFFFFFFWrite /dRotation to enable again."") 	
            end)

            SLASH_WHATEVERYOURFRAMESARECALLED1=""/dRotation""
            SlashCmdList.WHATEVERYOURFRAMESARECALLED = function()
                if dRotationFrame:IsShown() then
                    dRotationFrame:Hide()
                else
                    dRotationFrame:Show()
                end
            end
        end");
    }

    public void getRaidMembers()
    {
        Lua.LuaDoString(@"
            if UnitExists(""target"") 
            and UnitIsDead(""target"") == nil
            and UnitIsFriend(""player"", ""target"") 
            and UnitCanCooperate(""player"",""target"")
            and UnitInRange(""target"") ~= nil
            then PQR_CustomTarget = ""target""
            PQR_CustomTargetHP = 100 * UnitHealth(""target"") / UnitHealthMax(""target"")
            else 

            PQR_CustomTarget = ""player""
            PQR_CustomTargetHP = 100 * UnitHealth(""player"") / UnitHealthMax(""player"")
            local group = ""party""
            local members = GetNumPartyMembers()
            lowhpmembers = 0
            needfortitude = 0
            needshadow = 0
            needspirit = 0
            needshield = 0

            if GetNumRaidMembers() > 0 then
            group = ""raid""
            members = GetNumRaidMembers()
            end

            for i = 1, members, 1 do
            local member = group..tostring(i)
            local memberhp = 100 * UnitHealth(member) / UnitHealthMax(member)
            local haveshield = nil
            if UnitInRange(member) 
            and UnitIsFriend(""player"", member)
            and UnitHasVehicleUI(member) == false
            and UnitIsDeadOrGhost(member) == nil  then
            if UnitGroupRolesAssigned(member) == ""TANK"" then memberhp = memberhp - 1 end
            if UnitThreatSituation(member) == 3 then memberhp = memberhp - 3 end
            if UnitBuffID(member, 53563) ~= nil then memberhp = memberhp + 7 end
            if UnitBuffID(member, 48162) == nil and UnitBuffID(member, 48161) == nil then needfortitude = needfortitude + 1 end
            if UnitBuffID(member, 48170) == nil and UnitBuffID(member, 48169) == nil then needshadow = needshadow + 1 end
            if UnitBuffID(member, 48074) == nil and UnitBuffID(member, 48073) == nil then needspirit = needspirit + 1 end
            if UnitDebuffID(member, 6788) == nil then needshield = needshield + 1 end
            if UnitDebuffID(member, 6788) then haveshield = true end
            if memberhp < 85 then lowhpmembers = lowhpmembers +1 end
            if memberhp < PQR_CustomTargetHP then
            PQR_CustomTarget = member
            PQR_CustomTargetHP = memberhp
            end
            end
            end

        end
    ");
    }

    /* CombatRotation() */
    public void CombatRotation()
    {
        // Target Low HP
        getRaidMembers();

        // Flash Heal
        Lua.LuaDoString(@"
        if PQR_CustomTargetHP < 70
        then
            CastSpellByID(48071)
        end");

        // Shield
        Lua.LuaDoString(@"
        if UnitDebuffID(PQR_CustomTarget, 6788) == nil
            and UnitBuffID(PQR_CustomTarget, 48066) == nil
        then
            CastSpellByID(48066)
        end");
    }
}