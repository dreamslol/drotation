﻿using robotManager.Helpful;
using wManager.Wow.Enums;
using wManager.Wow.Helpers;
using wManager.Events;
using wManager.Wow.ObjectManager;
using System.Media;
using System.Windows.Forms;
using System;
using System.Threading;

public class Main_DWE : wManager.Plugin.IPlugin
{
    private bool _isRunning;

    /* Code here runs when the plugin is enabled and the bot it started. */
    public void Initialize()
    {
        _isRunning = true;
        Logging.Write("[DeathCounter] Loaded.");
        DeathCounter();
    }

    public static bool CheckIfInstance()
    {
        return Lua.LuaDoString<bool>("IsInInstance()");
    }

    public void DeathCounter()
    {
        //var IncreaseDeathCounter = wManager.Statistics.Deaths;

        while (_isRunning)
        {
            try
            {
                if (ObjectManager.Me.IsDead && CheckIfInstance())
                {
                    //IncreaseDeathCounter++;
                    wManager.Statistics.Deaths += 1;
                    Thread.Sleep(10000);
                }
            }
            catch (Exception e)
            {
                Logging.WriteError(" ERROR: " + e);
            }

            Thread.Sleep(10);
        }
    }

    public void Dispose()
    {
        _isRunning = false;
        Logging.Write("[DeathCounter] Stopped.");
    }

    public void Settings()
    {
        Logging.Write("[DeathCounter] No settings.");
    }
}