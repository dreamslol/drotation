﻿using System;
using System.Threading;
using robotManager.Helpful;
using robotManager.Products;
using wManager.Wow.Class;
using wManager.Wow.Enums;
using wManager.Wow.Helpers;
using wManager.Wow.ObjectManager;

public class Main : ICustomClass
{
    public string name = "dLeveling - Frost mage";
    public float Range { get { return 30.0f; } }

    private bool _isRunning;

    /*
     * Initialize()
     * When product started, initialize and launch Fightclass
     */
    public void Initialize()
    {
        _isRunning = true;
        Logging.Write(name + " Is initialized.");
        Buffs();
        RangeManager();
        Rotation();
    }

    /*
     * Dispose()
     * When product stopped
     */
    public void Dispose()
    {
        _isRunning = false;
        Logging.Write(name + " Stop in progress.");
    }

    /*
     * ShowConfiguration()
     * When use click on Fightclass settings
     */
    public void ShowConfiguration()
    {
        Logging.Write(name + " No settings for this Fightclass.");
    }

    /* Spells for Buffing */
    public Spell ArcaneIntellect = new Spell("Arcane Intellect");
    public Spell FrostArmor = new Spell("Frost Armor");
    public Spell IceBarrier = new Spell("Ice Barrier");

    // Food & Water
    public Spell ConjureFood = new Spell("Conjure Food");
    public Spell ConjureWater = new Spell("Conjure Water");

    /* Spells for Rotation */
    public Spell Blink = new Spell("Blink");
    public Spell FireBlast = new Spell("Fire Blast");
    public Spell FrostNova = new Spell("Frost Nova");
    public Spell IcyVeins = new Spell("Icy Veins");
    public Spell MirrorImage = new Spell("Mirror Image");
    public Spell Evocation = new Spell("Evocation");
    public Spell IceLance = new Spell("Ice Lance");
    public Spell DeepFreeze = new Spell("Deep Freeze");
    public Spell Frostbolt = new Spell("Frostbolt");
    public Spell Fireball = new Spell("Fireball");


    // Kiting
    private void RangeManager()
    {
        wManager.Events.FightEvents.OnFightLoop += (unit, cancelable) => {
            if ((ObjectManager.Target.HaveBuff("Frost Nova") || ObjectManager.Target.HaveBuff("Frostbite")) && ObjectManager.Target.HealthPercent >= 10 && ObjectManager.Target.GetDistance <= 8 && !Blink.IsSpellUsable)
            {
                /*
                var xvector = (ObjectManager.Me.Position.X) - (ObjectManager.Target.Position.X);
                var yvector = (ObjectManager.Me.Position.Y) - (ObjectManager.Target.Position.Y);

                Vector3 newpos = new Vector3()
                {
                    X = ObjectManager.Me.Position.X + (float)((xvector * (30 / ObjectManager.Target.GetDistance) - xvector)),
                    Y = ObjectManager.Me.Position.Y + (float)((yvector * (30 / ObjectManager.Target.GetDistance) - yvector)),
                    Z = ObjectManager.Me.Position.Z
                };
                MovementManager.Go(PathFinder.FindPath(newpos), false);
                */
                Move.Backward(Move.MoveAction.PressKey, 3000);
                Thread.Sleep(3200);
            }
        };
    }

    /* Rotation() */
    public void Rotation()
    {
        Logging.Write(name + ": Started.");

        while (_isRunning)
        {
            try
            {
                if (!Products.InPause)
                {
                    Buffs();
                    if (!ObjectManager.Me.IsDead && ObjectManager.Target.IsAttackable)
                    {
                        CombatRotation();
                    }
                }
            }
            catch (Exception e)
            {
                Logging.WriteError(name + " ERROR: " + e);
            }

            Thread.Sleep(10); // Pause 10 ms to reduce the CPU usage.
        }
        Logging.Write(name + ": Stopped.");
    }

    /* Buffs() */
    public void Buffs()
    {
        // Arcane Intellect
        if (ArcaneIntellect.KnownSpell && ArcaneIntellect.IsSpellUsable && !ObjectManager.Me.HaveBuff("Arcane Intellect"))
        {
            ArcaneIntellect.Launch();
            return;
        }

        // ice Armor
        if (FrostArmor.KnownSpell && FrostArmor.IsSpellUsable && !ObjectManager.Me.HaveBuff("Frost Armor"))
        {
            FrostArmor.Launch();
            return;
        }

        // Ice Barrier
        if (IceBarrier.KnownSpell && IceBarrier.IsSpellUsable && !ObjectManager.Me.HaveBuff("Ice Barrier"))
        {
            IceBarrier.Launch();
            return;
        }

        // Food
        if (ConjureFood.KnownSpell && ConjureFood.IsSpellUsable &&
            !(ItemsManager.HasItemById(8075)
            || ItemsManager.HasItemById(1487)
            || ItemsManager.HasItemById(1114)
            || ItemsManager.HasItemById(1113)
            || ItemsManager.HasItemById(8076)
            || ItemsManager.HasItemById(5349)
            || ItemsManager.HasItemById(22895)
            || ItemsManager.HasItemById(22019)))
        {
            ConjureFood.Launch();
            Thread﻿.Sleep(4000);
            return;
        }

        // Water
        if (ConjureWater.KnownSpell && ConjureWater.IsSpellUsable &&
            !(ItemsManager.HasItemById(30703)
            || ItemsManager.HasItemById(3772)
            || ItemsManager.HasItemById(8077)
            || ItemsManager.HasItemById(8078)
            || ItemsManager.HasItemById(2136)
            || ItemsManager.HasItemById(8079)
            || ItemsManager.HasItemById(2288)
            || ItemsManager.HasItemById(5350)
            || ItemsManager.HasItemById(22018)))
        {
            ConjureWater.Launch();
            Thread﻿.Sleep(4000);
            return;
        }
    }

    /* CombatRotation() */
    public void CombatRotation()
    {
        // Blink
        if (Blink.KnownSpell && Blink.IsSpellUsable && (ObjectManager.Target.HaveBuff("Frost Nova") || ObjectManager.Target.HaveBuff("Frostbite")) && ObjectManager.Target.HealthPercent >= 10 && ObjectManager.Target.GetDistance <= 8)
        {
            Blink.Launch();
            return;
        }

        // Fireblast
        if (FireBlast.KnownSpell && FireBlast.IsSpellUsable && ObjectManager.Target.IsAttackable && ObjectManager.Target.GetDistance <= 20 && !(ObjectManager.Target.HaveBuff("Frost Nova") || ObjectManager.Target.HaveBuff("Frostbite")))
        {
            FireBlast.Launch();
            return;
        }

        // Frostnova
        if (FrostNova.KnownSpell && FrostNova.IsSpellUsable && ObjectManager.Target.IsAttackable && ObjectManager.Target.GetDistance <= 8)
        {
            FrostNova.Launch();
            return;
        }

        // Ice Lance
        if (IceLance.KnownSpell && IceLance.IsSpellUsable && ObjectManager.Target.IsAttackable && ObjectManager.Target.HaveBuff("Deep Freeze"))
        {
            IceLance.Launch();
            return;
        }

        // Deepfreeze
        if (DeepFreeze.KnownSpell && DeepFreeze.IsSpellUsable && DeepFreeze.IsDistanceGood && ObjectManager.Target.IsAttackable)
        {
            DeepFreeze.Launch();
            return;
        }

        // Icy Veins
        if (IcyVeins.KnownSpell && IcyVeins.IsSpellUsable && ObjectManager.Target.IsAttackable)
        {
            IcyVeins.Launch();
            return;
        }

        // Mirror Image
        if (MirrorImage.KnownSpell && MirrorImage.IsSpellUsable && ObjectManager.Target.IsAttackable)
        {
            MirrorImage.Launch();
            return;
        }

        // Evocation
        if (Evocation.KnownSpell && Evocation.IsSpellUsable && ObjectManager.Target.IsAttackable && ObjectManager.Me.ManaPercentage <= 10)
        {
            Evocation.Launch();
            Thread﻿.Sleep(Usefuls.Latency + 6000);
            return;
        }

        // Fireball! Proc
        if (Fireball.KnownSpell && Fireball.IsSpellUsable && ObjectManager.Target.IsAttackable && ObjectManager.Me.HaveBuff("Fireball!"))
        {
            Fireball.Launch();
            return;
        }

        // Fireball - low level till Frostbolt
        if (!Frostbolt.KnownSpell && Fireball.IsSpellUsable && Fireball.IsDistanceGood && ObjectManager.Target.IsAttackable)
        {
            Fireball.Launch();
            return;
        }

        // Frostbolt
        if (Frostbolt.KnownSpell && Frostbolt.IsSpellUsable && Frostbolt.IsDistanceGood && ObjectManager.Target.IsAttackable)
        {
            Frostbolt.Launch();
            return;
        }
    }
}