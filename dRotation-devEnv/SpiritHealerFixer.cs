﻿using robotManager.Helpful;
using wManager.Wow.Enums;
using wManager.Wow.Helpers;
using wManager.Events;
using wManager.Wow.ObjectManager;
using System.Media;
using System.Windows.Forms;
using System;
using System.Threading;

public class Main_DA : wManager.Plugin.IPlugin
{
    private bool _isRunning;

    /* Code here runs when the plugin is enabled and the bot it started. */
    public void Initialize()
    {
        _isRunning = true;
        Logging.Write("[dDungeonAlerter] Loaded.");
        SpirithealerAccept();
    }

    /* Rotation() */
    public void SpirithealerAccept()
    {
        while (_isRunning)
        {
            try
            {
                if (ObjectManager.Me.HaveBuff(8326))
                {
                    Lua.LuaDoString("StaticPopup1Button1:Click()");
                }
            }
            catch (Exception e)
            {
                Logging.WriteError(" ERROR: " + e);
            }

            Thread.Sleep(10); // Pause 10 ms to reduce the CPU usage.
        }
    }

    public void Dispose()
    {
        _isRunning = false;
        Logging.Write("[dDungeonAlerter] Stopped.");
    }

    public void Settings()
    {
        Logging.Write("[dDungeonAlerter] No settings.");
    }
}